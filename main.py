import pygame
import random
import time
import math

random.seed(time)

pygame.init() 

display_width=800
display_height=600

black=(0,0,0)
brown=(138,54,15)
blue=(95,158,160)
swamp=(26,26,26)

thickness=math.floor(display_height/12)
land=math.floor(display_height/36)
water=math.ceil(25*display_height/216)
bar=land+water

score=0
landvalue=1
onland=True
steppedonland=False

def things(thingx, thingy):
    IMAGE = pygame.image.load('boat.png')
    rect = IMAGE.get_rect()
    rect = (thingx,thingy)
    gameDisplay.blit(IMAGE, rect)

pygame.display.set_caption("River Racer")
icon = pygame.image.load('sea.png')
pygame.display.set_icon(icon)

gameDisplay = pygame.display.set_mode((display_width, display_height))

clock=pygame.time.Clock()

player1img=pygame.image.load('player1img.png')
player1img_width=16

def player1(x,y):
    gameDisplay.blit(player1img, (x,y))

def backythegroundy():
    gameDisplay.fill(brown,(0,display_height-thickness,display_width,thickness))
    gameDisplay.fill(blue,(0,display_height-thickness-water,display_width,water))
    gameDisplay.fill(brown,(0,display_height-thickness-bar,display_width,land))
    gameDisplay.fill(blue,(0,display_height-thickness-bar-water,display_width,water))
    gameDisplay.fill(brown,(0,display_height-thickness-(2*bar),display_width,land))
    gameDisplay.fill(blue,(0,display_height-thickness-(2*bar)-water,display_width,water))
    gameDisplay.fill(brown,(0,display_height-thickness-(3*bar),display_width,land))
    gameDisplay.fill(blue,(0,display_height-thickness-(3*bar)-water,display_width,water))
    gameDisplay.fill(brown,(0,display_height-thickness-(4*bar),display_width,land))
    gameDisplay.fill(blue,(0,display_height-thickness-(4*bar)-water,display_width,water))
    gameDisplay.fill(brown,(0,display_height-thickness-(5*bar),display_width,land))
    gameDisplay.fill(blue,(0,display_height-thickness-(5*bar)-water,display_width,water))
    gameDisplay.fill(brown,(0,display_height-thickness-(5*bar)-water-thickness,display_width,thickness))

def fixedthings(fithingx, fithingy, fithingw, fithingh, ficolor):
    pygame.draw.rect(gameDisplay, ficolor, [fithingx, fithingy, fithingw, fithingh])

def text_objects(text, font):
    textSurface = font.render(text, True, black)
    return textSurface, textSurface.get_rect()
def playeronland(y):
    global onland
    global steppedonland
    global landvalue
    if y+14<= thickness:
        onland=True
        if landvalue != 1:
            steppedonland=True
        landvalue=1
    elif thickness+bar-3<y+14<thickness+bar:
        onland=True
        if landvalue != 2:
            steppedonland=True
        landvalue=2
    elif thickness+(2*bar)-3<y+14<thickness+(2*bar):
        onland=True
        if landvalue != 3:
            steppedonland=True
        landvalue=3
    elif thickness+(3*bar)-3<y+14<thickness+(3*bar):
        onland=True
        if landvalue != 4:
            steppedonland=True
        landvalue=4
    elif thickness+(4*bar)-3<y+14<thickness+(4*bar):
        onland=True
        if landvalue != 5:
            steppedonland=True
        landvalue=5
    elif thickness+(5*bar)-3<y+14<thickness+(5*bar):
        onland=True
        if landvalue != 6:
            steppedonland=True
        landvalue=6
    elif thickness+(5*bar)+water<y<display_width-14:
        onland=True
        if landvalue != 7:
            steppedonland=True
        landvalue=7

def text_objects(text, font):
    textSurface = font.render(text, True, black)
    return textSurface, textSurface.get_rect()


def crash():
    global initime
    finitime=pygame.time.get_ticks()
    timetaken=(finitime-initime)/1000
    text='you crashed,your score is'+str(score-timetaken+100)+' and level is '+str(level)
    largeText = pygame.font.Font('freesansbold.ttf',30)
    TextSurf, TextRect = text_objects(text, largeText)
    TextRect.center = ((display_width/2),(display_height/2))
    gameDisplay.blit(TextSurf, TextRect)
    pygame.display.update()
    time.sleep(2)
    game_loop()


def check(boaty,swimy):
    global waternumb
    if thickness<boaty<thickness+41 and thickness<swimy<thickness+water:
        waternumb=1
        return True
    elif thickness+bar<boaty<thickness+bar+41 and thickness+bar<swimy<thickness+bar+water:
        waternumb=2
        return True
    elif thickness+(2*bar)<boaty<thickness+(2*bar)+41 and thickness+(2*bar)<swimy<thickness+(2*bar)+water:
        waternumb=3
        return True
    elif thickness+(3*bar)<boaty<thickness+(3*bar)+41 and thickness+(3*bar)<swimy<thickness+(3*bar)+water:
        waternumb=4
        return True
    elif thickness+(4*bar)<boaty<thickness+(4*bar)+41 and thickness+(4*bar)<swimy<thickness+(4*bar)+water:
        waternumb=5
        return True
    elif thickness+(5*bar)<boaty<thickness+(5*bar)+41 and thickness+(5*bar)<swimy<thickness+(5*bar)+water:
        waternumb=6
        return True
    else:
        return False
level=1
def game_loop():
    global initime
    initime=pygame.time.get_ticks()
    global level
    global onland
    global steppedonland
    global landvalue
    global waternumb
    score=0
    onland=True
    landvalue=7
    steppedonland=False
    waternumb=0
    a=0
    b=0
    c=0
    d=0
    e=0
    f=0
    cross1=False
    cross3=False
    cross4=False
    cross7=False
    cross10=False
    x=(display_width * 0.5)-(player1img_width*0.5)
    y=display_height - player1img_width

    fix1=math.ceil((3*display_width)/16)
    fiy1=thickness+water
    fix2=math.ceil((5*display_width)/8)
    fiy2=thickness+water
    fix3=math.ceil(display_width/4)
    fiy3=thickness+bar+water
    fix4=math.ceil(display_width/6)
    fiy4=thickness+(2*bar)+water
    fix5=math.ceil(display_width/2)
    fiy5=thickness+(2*bar)+water
    fix6=math.ceil((5*display_width)/6)
    fiy6=thickness+(2*bar)+water
    fix7=math.ceil(display_width/4)
    fiy7=thickness+(3*bar)+water
    fix8=math.ceil(display_width/2)
    fiy8=thickness+(3*bar)+water
    fix9=math.ceil((3*display_width)/4)
    fiy9=thickness+(3*bar)+water
    fix10=math.ceil(display_width/8)
    fiy10=thickness+(4*bar)+water
    fix11=math.ceil((5*display_width)/8)
    fiy11=thickness+(4*bar)+water

    swampwidth=math.ceil(display_width/8)
    swamplength=land

    thing_startx1 = -55
    thing_startx2 = -55
    thing_startx3 = -55
    thing_startx4 = -55
    thing_startx5 = -55
    thing_startx6 = -55
    thing_starty1 = thickness+((random.randrange(0,6))*bar)+random.randrange(0,6)+land
    thing_starty2 = thickness+((random.randrange(0,6))*bar)+random.randrange(0,6)+land
    thing_starty3 = thickness+((random.randrange(0,6))*bar)+random.randrange(0,6)+land
    thing_starty4 = thickness+((random.randrange(0,6))*bar)+random.randrange(0,6)+land
    thing_starty5 = thickness+((random.randrange(0,6))*bar)+random.randrange(0,6)+land
    thing_starty6 = thickness+((random.randrange(0,6))*bar)+random.randrange(0,6)+land
    thing_speed1 = random.randrange(level,level+5)
    thing_speed2 = random.randrange(level,level+5)
    thing_speed3 = random.randrange(level,level+5)
    thing_speed4 = random.randrange(level,level+5)
    thing_speed5 = random.randrange(level,level+5)
    thing_speed6 = random.randrange(level,level+5) 


    x_change=0
    y_change=0

    gameExit = False

    while not gameExit:
        steppedonland=False
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

            if event.type==pygame.KEYDOWN:
                if event.key==pygame.K_LEFT:
                    if x<0:
                        x_change=0
                    else:
                        x_change=-5
                elif event.key==pygame.K_RIGHT:
                    if x>display_width-player1img_width:
                        x_change=0
                    else:
                        x_change=5
                elif event.key==pygame.K_UP:
                    if y<0:
                        y_change=0
                    else:
                        y_change=-1
                elif event.key==pygame.K_DOWN:
                    if y>display_height-player1img_width:
                        y_change=0
                    else:
                        y_change=1
            if event.type==pygame.KEYUP:
                if event.key==pygame.K_LEFT or event.key==pygame.K_RIGHT:
                    x_change=0
                elif event.key==pygame.K_UP or event.key==pygame.K_DOWN:
                    y_change=0
        x+=x_change
        y+=y_change

        if x<0 or x>display_width-player1img_width:
            x_change=0
        if y<0 or y>display_height-player1img_width:
            y_change=0

        x+=x_change
        y+=y_change
        playeronland(y)
        backythegroundy()
        player1(x,y)

        
        things(thing_startx1,thing_starty1)
        if x+20 > thing_startx1 and x < thing_startx1 + 51 :
            if y+18 > thing_starty1 and y<thing_starty1+41:
                crash()
                game_loop()
        thing_startx1 += thing_speed1
        if check(thing_starty1,y):
            if thing_startx1 < x:
                a=1
        if steppedonland and landvalue==waternumb:
            if a==1:   
                score=score+10
                print(score)
            a=0
        if thing_startx1 > display_width :            
            thing_startx1 = -55
            thing_starty1 = thickness+((random.randrange(0,6))*bar)+random.randrange(0,6)+land
            thing_speed1 = random.randrange(level,level+5)
            


        things(thing_startx2,thing_starty2)
        if x+20 > thing_startx2 and x < thing_startx2 + 51 :
            if y+18 > thing_starty2 and y<thing_starty2+41:
                crash()
                game_loop()
        thing_startx2 += thing_speed2
        if check(thing_starty2,y):
            if thing_startx2 < x:
                b=1
        if steppedonland and landvalue==waternumb:
            if b==1:   
                score=score+10
                print(score)
            b=0
        if thing_startx2 > display_width :            
            thing_startx2 = -55
            thing_starty2 = thickness+((random.randrange(0,6))*bar)+random.randrange(0,6)+land
            thing_speed2 = random.randrange(level,level+5)
            


        things(thing_startx3,thing_starty3)
        if x+20 > thing_startx3 and x < thing_startx3 + 51 :
            if y+18 > thing_starty3 and y<thing_starty3+41:
                crash()
                game_loop()
        thing_startx3 += thing_speed3
        if check(thing_starty3,y):
            if thing_startx3 < x:
                c=1
        if steppedonland and landvalue==waternumb:
            if c==1:   
                score=score+10
                print(score)
            c=0
        if thing_startx3 > display_width:           
            thing_startx3 = -55
            thing_starty3 = thickness+((random.randrange(0,6))*bar)+random.randrange(0,6)+land
            thing_speed3 = random.randrange(level,level+5)
            

        things(thing_startx4,thing_starty4)
        if x+20 > thing_startx4 and x < thing_startx4 + 51 :
            if y+18 > thing_starty4 and y<thing_starty4+41:
                crash()
                game_loop()
        thing_startx4 += thing_speed4
        if check(thing_starty4,y):
            if thing_startx4 < x:
                d=1
        if steppedonland and landvalue==waternumb:
            if d==1:   
                score=score+10
                print(score)
            d=0
        if thing_startx4 > display_width:            
            thing_startx4 = -55
            thing_starty4 = thickness+((random.randrange(0,6))*bar)+random.randrange(0,6)+land
            thing_speed4 = random.randrange(level,level+5)
            


        things(thing_startx5,thing_starty5)
        if x+20 > thing_startx5 and x < thing_startx5 + 51 :
            if y+18 > thing_starty5 and y<thing_starty5+41:
                crash()
                game_loop()
        thing_startx5 += thing_speed5
        if check(thing_starty5,y):
            if thing_startx5 < x:
                e=1
        if steppedonland and landvalue==waternumb:
            if e==1:   
                score=score+10
                print(score)
            e=0
        if thing_startx5 > display_width:
            thing_startx5 = -55
            thing_starty5 = thickness+((random.randrange(0,6))*bar)+random.randrange(0,6)+land
            thing_speed5 = random.randrange(level,level+5)
            


        things(thing_startx6,thing_starty6)
        if x+20 > thing_startx6 and x < thing_startx6 + 51 :
            if y+18 > thing_starty6 and y<thing_starty6+41:
                crash()
                game_loop()
        thing_startx6 += thing_speed6
        if check(thing_starty6,y):
            if thing_startx6 < x:
                f=1
        if steppedonland and landvalue==waternumb:
            if f==1:   
                score=score+10
                print(score)
            f=0
        if thing_startx6 > display_width:
            thing_startx6 = -55
            thing_starty6 = thickness+((random.randrange(0,6))*bar)+random.randrange(0,6)+land
            thing_speed6 = random.randrange(level,level+5)
            
        
        
        
        
        
        
        

        fixedthings(fix1,fiy1,swampwidth,swamplength,swamp)
        if x+16 >fix1 and x<fix1+100:
            if y+14 > fiy1 and y<fiy1+land:
                crash()
                game_loop()
        if y==fiy1:
            if not cross1:
                score=score+10
                print(score)
                cross1=True
        
        fixedthings(fix2,fiy2,swampwidth,swamplength,swamp)
        if x+16 >fix2 and x<fix2+100:
            if y+14 > fiy2 and y<fiy2+land:
                crash()
                game_loop()
        
        fixedthings(fix3,fiy3,(2*swampwidth),swamplength,swamp)
        if x+16 >fix3 and x<fix3+200:
            if y+14 > fiy3 and y<fiy3+land:
                crash()
                game_loop()
        if y==fiy3:
            if not cross3:
                score=score+5
                print(score)
                cross3=True
        fixedthings(fix4,fiy4,swampwidth,swamplength,swamp)
        if x+16 >fix4 and x<fix4+100:
            if y+14 > fiy4 and y<fiy4+land:
                crash()
                game_loop()
        if y==fiy4:
            if not cross4:
                score=score+15
                print(score)
                cross4=True
        fixedthings(fix5,fiy5,swampwidth,swamplength,swamp)
        if x+16 >fix5 and x<fix5+100:
            if y+14 > fiy5 and y<fiy5+land:
                crash()
                game_loop()
        fixedthings(fix6,fiy6,swampwidth,swamplength,swamp)
        if x+16 >fix6 and x<fix6+100:
            if y+14 > fiy6 and y<fiy6+land:
                crash()
                game_loop()
        fixedthings(fix7,fiy7,swampwidth,swamplength,swamp)
        if x+16 >fix7 and x<fix7+100:
            if y+14 > fiy7 and y<fiy7+land:
                crash()
                game_loop()
        if y==fiy7:
            if not cross7:
                score=score+15
                print(score)
                cross7=True
        fixedthings(fix8,fiy8,swampwidth,swamplength,swamp)
        if x+16 >fix8 and x<fix8+100:
            if y+14 > fiy8 and y<fiy8+land:
                crash()
                game_loop()
        fixedthings(fix9,fiy9,swampwidth,swamplength,swamp)
        if x+16 >fix9 and x<fix9+100:
            if y+14 > fiy9 and y<fiy9+land:
                crash()
                game_loop()
        fixedthings(fix10,fiy10,swampwidth,swamplength,swamp)
        if x+16 >fix10 and x<fix10+100:
            if y+14 > fiy10 and y<fiy10+land:
                crash()
                game_loop()
        if y==fiy10:
            if not cross10:
                score=score+10
                print(score)
                cross10=True
        fixedthings(fix11,fiy11,swampwidth,swamplength,swamp)
        if x+16 >fix11 and x<fix11+100:
            if y+14 > fiy11 and y<fiy11+land:
                crash()
                game_loop()


        clock.tick(30)
        pygame.display.update()
        if y<5:
            level=level+1
            game_loop()

game_loop()



